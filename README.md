(0)
IntelliJ - install proto buffer plugin for syntax highlights

(1)
introduce project structure

(2)
add gradle dependencies
    search a suitable ${grpcVersion} (e.g. 1.52.1) on https://mvnrepository.com/artifact, and add dependency for<br/> 
- `runtimeOnly "io.grpc:grpc-netty-shaded:${grpcVersion}"`
- `implementation "io.grpc:grpc-protobuf:${grpcVersion}"`
- `implementation "io.grpc:grpc-stub:${grpcVersion}"`
- `compileOnly "javax.annotation:javax.annotation-api:1.3.2" // necessary for Java 9+`
- `testImplementation "io.grpc:grpc-testing:${grpcVersion}"`

(3)
gradle plugin for compiling protobuf files (the service interface definitions)<br/>
    search on https://mvnrepository.com/artifact, for
- `protobuf-gradle-plugin`
- `com.google.protobuf:protoc`
- `io.grpc:protoc-gen-grpc-java`

add to the gradle build file,
```
    plugins {
        id 'com.google.protobuf' version '0.8.18'
    }

    protobuf {
        protoc {
            artifact = "com.google.protobuf:protoc:${protocVersion}"
        }

        plugins {
            grpc {
                artifact = "io.grpc:protoc-gen-grpc-java:${grpcVersion}"
            }
        }

        generateProtoTasks {
            all()*.plugins {
                grpc {}
            }
        }
    }

    sourceSets {
        main {
            java {
                srcDirs 'build/generated/source/proto/main/grpc'
                srcDirs 'build/generated/source/proto/main/java'
            }
        }
    }
```

(5)
create interface definition - prototype buffer (.proto) file
    `src/main/proto/book.proto`
    `src/main/proto/borrower.proto`
    `src/main/proto/borrowing-record.proto`

    e.g.
```
    service BorrowerService {
        rpc getBorrowerDetails(GetBorrowerDetailsRequest) returns (GetBorrowerDetailsResponse);
        rpc getBorrowingRecord(GetBorrowingRecordRequest) returns (GetBorrowingRecordResponse);
    }

    message GetBorrowerDetailsRequest {
        string borrowerCardNumber = 1;
    }

    ......
```

(6)
run gradle build - generate interface files

(7)
create server and service
- Book
  - db - entity, DAO (existing)
  - server - BookServer
  - service - BookService extends BookServiceGrpc.BookServiceImplBase
- BorrowingRecord
  - db
  - server - inject DateUtil
  - service
<br/>

Similarly, create service implementation and server for Borrower<br/>

(8)
Call from Borrower.getBorrowingRecord to Borrower-Record and Book
- create service adapter
  - `serviceadapter/BookServiceAdapter.java`
  - `serviceadapter/BorrowingRecordServiceAdapter.java`
  - `serviceadapter/factory/ServiceAdapterFactory.java`
    - get a channel
    - build stub
    - call service
    - map response back to the current service object

N
Unit tests

N+1
Testing with grpcurl

N+2
Next
    Secure connection
    Authentication and Authorization
    Asynchronous request instead of unary
    Orchestration
    Discovery and load balancing
