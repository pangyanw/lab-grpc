package io.pangyanw.library.borrowingrecord.service;

import io.grpc.stub.StreamObserver;
import io.pangyanw.library.borrowingrecord.db.BorrowingRecord;
import io.pangyanw.library.borrowingrecord.db.BorrowingRecordDao;
import io.pangyanw.library.date.DateUtil;
import io.pangyanw.library.stubs.borrowing_record.BorrowingRecordServiceGrpc;
import io.pangyanw.library.stubs.borrowing_record.GetBorrowingRecordRequest;
import io.pangyanw.library.stubs.borrowing_record.GetBorrowingRecordResponse;

import java.util.List;
import java.util.stream.Collectors;

public class BorrowingRecordServiceImpl extends BorrowingRecordServiceGrpc.BorrowingRecordServiceImplBase {

    private final DateUtil dateUtil;

    public BorrowingRecordServiceImpl(DateUtil dateUtil) {
        this.dateUtil = dateUtil;
    }

    @Override
    public void getBorrowingRecord(GetBorrowingRecordRequest request, StreamObserver<GetBorrowingRecordResponse> responseStreamObserver) {
        BorrowingRecordDao borrowingRecordDao = new BorrowingRecordDao(dateUtil);
        List<BorrowingRecord> borrowingRecords = borrowingRecordDao.getBorrowingRecord(request.getBorrowerId());

        List<io.pangyanw.library.stubs.borrowing_record.BorrowingRecord> borrowingRecordsInResponse = borrowingRecords.stream().map(borrowingRecord -> io.pangyanw.library.stubs.borrowing_record.BorrowingRecord.newBuilder()
                        .setBorrowerId(borrowingRecord.getBorrowerId())
                        .setBookId(borrowingRecord.getBookId())
                        .setDueDate(borrowingRecord.getDueDate())
                        .build())
                .collect(Collectors.toList());

        GetBorrowingRecordResponse.Builder borrowingRecordResponseBuilder = GetBorrowingRecordResponse.newBuilder()
                .addAllBorrowingRecord(borrowingRecordsInResponse);

        GetBorrowingRecordResponse borrowingRecordResponse = borrowingRecordResponseBuilder.build();

        responseStreamObserver.onNext(borrowingRecordResponse);
        responseStreamObserver.onCompleted();
    }
}
